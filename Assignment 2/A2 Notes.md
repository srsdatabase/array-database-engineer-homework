# Assignment 2 Notes
_Not long after the initial launch, the CTO comes to you and asks to build a report showing the number of 3B TransUnion reports ordered per month, for each client._
_Using the schema you designed above, what would be the query you’d use to retrieve this information?_
- - - -
For this exercise, I made an assumption that a 3B order is not a product but rather some type of indicator at the **order** level and modeled accordingly. The following #queries gather counts based on this indicator.

`-- Example 1: Simple query to return required results`
```
SELECT cl.ClientName AS [Client Name],
       FORMAT(co.OrderDate, 'MMM yyyy') AS [Order Month],
       COUNT(co.CustomerOrderID) AS [B3 Order Count]
FROM   dbo.CustomerOrder co
INNER  JOIN dbo.Customer cu
ON     co.CustomerID = cu.CustomerID
AND    co.Is3BOrder = 1
INNER  JOIN dbo.Client cl
ON     cu.ClientID = cl.ClientID
GROUP  BY cl.ClientName,
       FORMAT(co.OrderDate, 'MMM yyyy')
ORDER  BY cl.ClientName,
       [Order Month]
```

However, since I changed the format of the month label, to make it more user-friendly, I lost the ability to sort in chronological order. Let’s modify this query slightly to pass the month number from a #CTE to the main query in order to have more control of the sorting.

`-- Example 2: Use a CTE to have better control over sort order`
```
WITH b3_orders AS (
   SELECT cl.ClientName,
          FORMAT(co.OrderDate, 'MM') AS OrderMonthNumber,
          FORMAT(co.OrderDate, 'MMM') AS OrderMonthName,
          FORMAT(co.OrderDate, 'yyyy') AS OrderYear,
          COUNT(co.CustomerOrderID) AS B3OrderCount
   FROM   dbo.CustomerOrder co
   INNER  JOIN dbo.Customer cu
   ON     co.CustomerID = cu.CustomerID
   AND    co.Is3BOrder = 1
   INNER  JOIN dbo.Client cl
   ON     cu.ClientID = cl.ClientID
   GROUP  BY cl.ClientName,
          FORMAT(co.OrderDate, 'MM'),
          FORMAT(co.OrderDate, 'MMM'),
          FORMAT(co.OrderDate, 'yyyy')
)
SELECT ClientName AS [Client Name],
       CAST(OrderMonthName AS VARCHAR(9)) + ' ' + CAST(OrderYear AS CHAR) AS [Order Month],
       B3OrderCount AS [B3 Order Count]
FROM   b3_orders       
ORDER  BY ClientName,
       OrderYear DESC,
       OrderMonthNumber DESC
```

Now, just for fun, let’s assume that the requestor of this report doesn’t have any knowledge of Excel (or other tool) pivot capabilities or analytic functions, and they have requested me to produce a pivot report for them using just a query. In the following example, we’ll store the counts of 3B orders in a temporary table. Next, we’ll create a dummy set of headers that we can pass into a dynamic query that uses the PIVOT function to match the report month and year with the dynamically-generated header list.

`— Example 3: Just for fun, create pivot using dynamic SQL to generate column list of last x number of months`
```
DROP TABLE IF EXISTS #b3_orders;

SELECT cl.ClientName,
       FORMAT(co.OrderDate, 'MM') AS OrderMonthNumber,
       FORMAT(co.OrderDate, 'MMM') AS OrderMonthName,
       FORMAT(co.OrderDate, 'yyyy') AS OrderYear,
       co.CustomerOrderID AS B3Order
INTO   #b3_orders       
FROM   dbo.CustomerOrder co
INNER  JOIN dbo.Customer cu
ON     co.CustomerID = cu.CustomerID
AND    co.Is3BOrder = 1
INNER  JOIN dbo.Client cl
ON     cu.ClientID = cl.ClientID;

DECLARE @header_list VARCHAR(250);
DECLARE @pivot_query VARCHAR(MAX);

WITH month_sequence AS (
   SELECT 0 AS Sequence
   UNION ALL
   SELECT Sequence - 1
   FROM   month_sequence
   WHERE Sequence > -6
)

SELECT @header_list = STUFF((SELECT ',' + '[' + FORMAT(DATEADD(MONTH, Sequence, GETDATE()), 'MMM') + ' ' + FORMAT(DATEADD(MONTH, Sequence, GETDATE()), 'yyyy') + ']'
FROM   month_sequence
WHERE  1 = 1
FOR    XML PATH ('')), 1, 1, '')
-- SELECT @header_list

SET @pivot_query = '
   SELECT * FROM (
         SELECT ClientName AS [Client Name],
                OrderMonthName + '' '' + OrderYear AS [Order Month],
                COUNT(1) as [B3 Order Count]
         FROM   #b3_orders
         GROUP BY ClientName, OrderMonthName + '' '' + OrderYear) b3
      PIVOT
      (
         SUM([B3 Order Count])
         FOR [Order Month] in (' + @header_list + ')
      ) p
      ORDER BY 1'

EXEC (@pivot_query);       
```