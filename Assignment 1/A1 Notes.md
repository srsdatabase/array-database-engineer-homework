# Assignment 1 Notes
_Imagine for a moment you're helping to design the initial schema for our white label portal product that we offer to clients._
_Customers log into the portal, branded to a particular client and are able to order credit reports. A total of 6 products are offered: Partial and Full reports for each of the three credit bureaus (Equifax, Experian, and TransUnion). We then bill our clients based on the number of each report their customers order._
_Describe the data schema for these entities. Include entity names, properties, data types, relationships and any relevant indexes, keys and constraints._
- - - -

For this exercise, I focused on the physical #data model#, but I wanted to bring attention to the benefits of starting with a logical (and even conceptual) model. I have included a basic entity relationship diagram in this assignment. Among other benefits, logical models:
* Promote reusability (utilizing data domains, for example)
* Provide business process documentation, impact analysis, and lineage
* Simplify the task of porting a physical data model to other RDBM systems

Assumptions:
1. Customized portal configuration for each client is likely to be a complex function with multiple tables and additional columns. This is outside the scope of this assignment, and I have shown only an abridged representation of a possible approach to this requirement.
2. The character data types in this model assume a Western language representation supporting only a single language. If support were needed for multiple languages, name, descriptions, and other language-specific text could be modeled in separate tables that include a language indicator. The national character sets (NCHAR or NVARCHAR, for example) would replace the existing character types.
3. There has always been heated debate about model naming standards. Some camps advocate a primary key column called simply ID while others prefer TableName + ID. Some believe firmly in singular table names while others use plural. For me, consistency is more important than a dogmatic approach. A standard should be established and documented for the department or project, and all developers should adhere.
4. For a large data set and if the customer-to-client relationship is immutable, I would denormalize the ClientID in the CustomerOrder table (and perhaps even CustomerOrderDetail) since it seems that the majority of reporting will be done at the Client level.