# README
## Introduction
Hi Array Team!

My name is Shane Stucky and I’m interested in a Database Engineer position with your company. I had the privilege of speaking with John Pirog several days ago, and based on our conversation, I am extremely excited about this opportunity.

This homework assignment has been a lot of fun for me, and it includes activities that I am passionate about and love doing. I hope you enjoy my presentation, and I thank you for your time and consideration.

## Project Organization
Because of the length of each assignment video (I have a lot to say!), I chose not to combine the video segments into a single video in an effort make each assignment more readily consumable.

Please start with the Introduction video in the main project directory. Videos, notes, and artifacts for each subsequent assignment are located in folders named with the assignment number.

Thanks again!

Cheers,
Shane

### External Links to Assignment Videos:
[Introduction](https://www.loom.com/share/e0b8701e8c634649bf52b51cbdbf887c)
[Assignment 1](https://www.loom.com/share/75f60fbbe386467f87c521004d15cc93)
[Assignment 2](https://www.loom.com/share/7050cfa627654f14a31d234ceb027c64)
[Assignment 3](https://www.loom.com/share/16be7196a4934c859c1927cd8c070f54)
[Assignment 4](https://www.loom.com/share/36dd9e1e724d4721980564fb6c4dd8c8)
[Assignment 5](https://www.loom.com/share/01b644e4e2584d24adf7ed49eae01b36)

