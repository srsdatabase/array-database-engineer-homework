# Assignment 5 Notes
_You discover the SQL database performance is being impacted by many concurrent long running queries._
_Describe your approach in how you'd diagnose, test and resolve the issue. Detail the tools and SQL statements you'd execute._
- - - -

Did this problem suddenly start happening,? If so, what recently changed? New code? More users? New data structures?

Look at overall current health using Activity Monitor

Check for stale statistics using sys.sysindexes query:

`— Check for stale statistics`
```
SELECT DB_NAME() AS [Database Name],
       SCHEMA_NAME(t.[schema_id]) AS [Schema Name],
       t.name AS [Table Name],
       ix.name AS [Index Name],
       STATS_DATE(ix.id, ix.indid) AS [Statistics Last Update],
       ix.rowcnt AS '[Row Count]',
       ix.rowmodctr AS [# Rows Changed],
       CAST((CAST(ix.rowmodctr AS DECIMAL(20,8))/CAST(ix.rowcnt AS DECIMAL(20,2)) * 100.0) AS DECIMAL(20,2)) AS [% Rows Changed]
FROM   sys.sysindexes ix
INNER  JOIN sys.tables t ON t.[object_id] = ix.[id]
WHERE  ix.id > 100 -- excluding system object statistics
AND    ix.indid > 0 -- excluding heaps or tables that do not any indexes
AND    ix.rowcnt >= 500 -- only indexes with more than 500 rows
ORDER  BY [% Rows Changed] DESC
```

Check for top 10 or 20 longest running #queries using says.dm_exec_query_stats:

`-- Get top 20 longest running queries`
```
SELECT DISTINCT TOP 20
       est.TEXT AS [Query],
       DB_NAME(dbid),
       eqs.execution_count AS [Execution Count],
       eqs.max_elapsed_time AS [Max Elapsed Time],
       ISNULL(eqs.total_elapsed_time / NULLIF(eqs.execution_count,0), 0) AS [Average Elapsed Time],
       eqs.creation_time AS [Creation Time],
       ISNULL(eqs.execution_count / NULLIF(DATEDIFF(s, eqs.creation_time, GETDATE()),0), 0) AS [Executions per Second],
       total_physical_reads AS [Total Physical Reads]
FROM   sys.dm_exec_query_stats eqs
CROSS  APPLY sys.dm_exec_sql_text(eqs.sql_handle) est
ORDER  BY eqs.max_elapsed_time DESC
```

Most importantly, identify the offending queries and tune them using indexing and query rewrite strategies:
* Use SQL Server Profiler to set up a timed trace and filter for queries that take longer than x seconds to run
* Use Extended Events (my preference!)
*  Create an Extended Event to capture Plan Handle

```
CREATE EVENT SESSION [LongRunners] ON SERVER 
ADD EVENT sqlserver.query_thread_profile(
    ACTION(sqlos.task_time,sqlserver.database_name,sqlserver.plan_handle,sqlserver.session_id,sqlserver.sql_text)),
ADD EVENT sqlserver.rpc_completed(SET collect_data_stream=(1)
    ACTION(sqlserver.database_name,sqlserver.plan_handle,sqlserver.session_id,sqlserver.sql_text)
    WHERE ((([package0].[greater_than_uint64]([sqlserver].[database_id],(4))) AND ([package0].[equal_boolean]([sqlserver].[is_system],(0)))) AND ([package0].[greater_than_equal_uint64]([sqlos].[task_execution_time],(2000))))),
ADD EVENT sqlserver.sql_batch_completed(
    ACTION(sqlserver.database_name,sqlserver.plan_handle,sqlserver.session_id,sqlserver.sql_text)
    WHERE ((([package0].[greater_than_uint64]([sqlserver].[database_id],(4))) AND ([package0].[equal_boolean]([sqlserver].[is_system],(0)))) AND ([package0].[greater_than_equal_uint64]([sqlos].[task_execution_time],(2000)))))
ADD TARGET package0.ring_buffer
WITH (MAX_MEMORY=4096 KB,EVENT_RETENTION_MODE=ALLOW_SINGLE_EVENT_LOSS,MAX_DISPATCH_LATENCY=30 SECONDS,MAX_EVENT_SIZE=0 KB,MEMORY_PARTITION_MODE=NONE,TRACK_CAUSALITY=ON,STARTUP_STATE=OFF)
GO

-- View complete execution plan for offending query
SELECT *
FROM   sys.dm_exec_query_plan(0x0500FF7FB95D60FFD02203558501000001000000000000000000000000000000000000000000000000000000)
```


* Use freely available open-source monitoring tools like Adam Machanic’s sp_whoisactive to log blocking sessions, wait stats
* Use enterprise-level tools like Dynatrace to identify specific application- or database-level components that are causing performance bottlenecks

