# Assignment 3 Notes
_One of our clients provides a data dump of customers they’d like us to add to our system. The format is a .csv file with 1,345 lines._
_Describe your approach in validating, formatting and importing the data into your data storage. Detail the tools and SQL statements you’d execute._
- - - -
There are many ways to accomplish this. I have demonstrated the SQL BULK INSERT command in the video and included sample #queries below, but this isn’t the method I typically use. I prefer a client utility such as SSMS flat-file import, or a 3rd party tool interface such as DBVisualizer.

One thing I failed to discuss in the video is the usage of a FORMATFILE to identify a specific set and order of columns. For simplicity, I ensured that every column in my Customer table was included in the .csv in the same order as in the table, but this is not usually the case.

Depending on the volume of data, the requirements for transformation, and the frequency of data load (or change), this may also be a candidate for a formal SSIS package.

For my example, I added a new Client with ID 105 and then loaded a Customer csv file containing 1345 records associated with this Client ID.

`-- View newly-added Client record`
```
SELECT *
FROM   dbo.Client
WHERE  ClientID = 105
```
`-- Validate that there are currently no customers associated with Client ID 105`
```
SELECT COUNT(*)
FROM   dbo.Customer
WHERE  ClientID = 105
```
`-- Load .csv file to existing Customer table`
```
BULK INSERT dbo.Customer
FROM 'C:\ArrayHW\Assignment3\A3_customer_file.csv'
WITH (
   FIRSTROW = 2,
   FIELDTERMINATOR = ',',
   ROWTERMINATOR = '\n',
   ERRORFILE = 'C:\ArrayHW\Assignment3\A3_customer_file',
   TABLOCK
)
```
`-- Validate that there are now 1345 customers associated with Client ID 105`
```
SELECT COUNT(*)
FROM   dbo.Customer
WHERE  ClientID = 105
```
