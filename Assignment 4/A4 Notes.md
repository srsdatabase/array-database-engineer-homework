# Assignment 4 Notes
_You’ve been asked to demonstrate to a junior engineer the use of CTEs. Write a query that would identify clients that have done over 100 reports in the last 30 days. The query should return the client’s name and the number of reports within the last 30 days._
- - - -
Common table expressions are an indispensable tool in my arsenal. They help me organize my approach and throughs to complex #queries, and they are invaluable for testing because they provide the ability to break down a large and complex query into more atomic units.

For this exercise, note that I didn’t include sufficient test data to satisfy the assignment condition of “more than 100 reports”, but the filter condition in the main query below can easily be adjusted to any number without modifying the #CTE or the structure of the query.

`-- Use a CTE to get count of reports from last 30 days for all clients`
```
WITH last_30 AS (
   SELECT cl.ClientName,
          COUNT(co.CustomerOrderID) AS OrderCount
   FROM   dbo.Client cl
   INNER  JOIN dbo.Customer cu
   ON     cl.ClientID = cu.ClientID
   INNER  JOIN dbo.CustomerOrder co
   ON     cu.CustomerID = co.CustomerID
   AND    co.OrderDate >= DATEADD(DAY, -30, GETDATE())
   INNER  JOIN dbo.CustomerOrderDetail cod
   ON     co.CustomerOrderID = cod.CustomerOrderID
   GROUP  BY cl.ClientName
)
-- Now show only the ones that have more than 100
SELECT ClientName,
       OrderCount
FROM   last_30
WHERE  OrderCount > 100       
```